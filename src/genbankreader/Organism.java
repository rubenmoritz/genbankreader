package genbankreader;

/**
 *
 * @author rjgmoritz A class that gives info about a certain organism giving a
 * input string.
 */
public class Organism {

    StringManipulation stringManipulation = new StringManipulation();

    /**
     *
     * @param genebankString the input file
     * @return the organism
     */
    public String sourceOrganism(String genebankString) {
        return stringManipulation.stringCompare(genebankString, "(.*)" + "organism" + "(.*)", false, null);
    }

    /**
     *
     * @param genebankString the input file
     * @return the CDS coordinates
     */
    public String cdsCoordinates(String genebankString) {
        return stringManipulation.stringCompare(genebankString, "(.*)" + "CDS" + "(.*)", true, null);
    }

    /**
     *
     * @param genebankString the input file
     * @return the CDS coordinates in an array
     */
    public String[] cdsCoordinatesAray(String genebankString) {
        String cooridinates = cdsCoordinates(genebankString);
        String[] coordAray = cooridinates.split("\n");
        return coordAray;
    }

    /**
     *
     * @param genebankString the input file
     * @return the CDS product
     */
    public String cdsProduct(String genebankString) {
        return stringManipulation.stringCompare(genebankString, "(.*)" + "(\\/)product" + "(.*)", true, null);
    }

    /**
     *
     * @param genebankString the input file
     * @return the CDS product in an array
     */
    public String[] cdsProductAray(String genebankString) {
        String products = cdsProduct(genebankString);
        String[] productAray = products.split("\n");
        return productAray;
    }

    /**
     *
     * @param genebankString the input file
     * @return the CDS xref
     */
    public String cdsDb_xref(String genebankString) {
        return stringManipulation.stringCompare(genebankString, "(.*)" + "db_xref" + "(.*)", true, null);
    }

    /**
     *
     * @param genebankString the input file
     * @return the CDS xref in an array
     */
    public String[] cdsDb_xrefAray(String genebankString) {
        String xrefs = cdsDb_xref(genebankString);
        String[] xrefsAray = xrefs.split("\n");
        return xrefsAray;
    }

    /**
     *
     * @param genebankString the input file
     * @return the unpacks the CDS Blocks and returns it
     */
    public String cdsBlockUnpacker(String genebankString) {
        return stringManipulation.stringCompare(genebankString, "(.*)" + "CDS" + "(.*)", true, "( {5})(\\w{3,})(.*)");
    }

    /**
     *
     * @param genebankString the input file
     * @return the unpacks the gene Blocks and returns it
     */
    public String geneBlockUnpacker(String genebankString) {
        return stringManipulation.stringCompare(genebankString, "( {5})" + "gene" + "(.*)", true, "(.*)(\\/)locus_tag(.*)");
    }

    /**
     *
     * @param genebankString the input file/string
     * @return the given string and adds "/gene=none" if the gene does not has a
     * name
     */
    public String insertMissingGene(String genebankString) {
        String[] lines = genebankString.split("\n");
        String withInsertedGenes = "";
        Boolean geneFound = false;
        for (int i = 0; lines.length > i + 1; i++) {

            if (lines[i].contains("/gene=none")) {
                geneFound = true;
            }
            if (lines[i + 1].matches("( {5})" + "gene" + "(.*)") && geneFound == false) {
                withInsertedGenes = withInsertedGenes + "/gene=\n";
            }
            if (lines[i + 1].matches("( {5})" + "gene" + "(.*)")) {
                geneFound = false;
            }
            withInsertedGenes = withInsertedGenes + lines[i] + "\n";
        }
        withInsertedGenes = withInsertedGenes + lines[lines.length - 1] + "\n";
        return withInsertedGenes;
    }

    /**
     *
     * @param genebankString the input file
     * @return the cds Translation
     */
    public String cdsTranslation(String genebankString) {
        return stringManipulation.stringCompare(genebankString, "(.*)" + "(\\/)translation" + "(.*)", true, "(.*)[A-Z]*\"(.*)");
    }

    /**
     *
     * @param genebankString the input file
     * @return the cds Translation in an array
     */
    public String[] cdsTranslationAray(String genebankString) {
        String translation = cdsTranslation(genebankString);
        String[] translationAray = translation.split("\"\n");
        return translationAray;
    }

    /**
     *
     * @param genebankString the input string
     * @param productString the product string (gene name) prints the nucleotide
     * sequence for the given gene
     */
    public void fetchGenes(String genebankString, String productString) {
        String geneBlocks = geneBlockUnpacker(genebankString);
        String[] geneGene = geneGene(insertMissingGene(geneBlocks)).split("\n");
        String[] coordinates = geneCoordinates(geneBlocks).split("\n");
        for (int i = 1; i < coordinates.length; i++) {

            if (geneGene[i].contains(productString)) {
                String[] looseCoords = stringManipulation.removeSignsFromString(stringManipulation.removeWordsFromString(coordinates[i])).split("\\.\\.");
                System.out.println("Found: " + geneGene[i] + " with " + looseCoords[0] + " and " + looseCoords[1]);
                System.out.println("giving: " + stringManipulation.removeStringFromString(sequence(genebankString), " ").substring(Integer.parseInt(looseCoords[0]), Integer.parseInt(looseCoords[1])));
            }
        }
    }

    /**
     *
     * @param genebankString the input string
     * @param productString the product string prints the amino acid sequence of
     * the given product
     */
    public void fetchCds(String genebankString, String productString) {
        String cdsBlocks = cdsBlockUnpacker(genebankString);
        String[] cdsProductAray = cdsProductAray(cdsBlocks);
        String[] cdsTranslationAray = cdsTranslationAray(cdsBlocks);
        for (int i = 0; i < cdsProductAray.length; i++) {
            if (cdsProductAray[i].contains(productString)) {
                System.out.println("Found: " + cdsProductAray[i] + " with " + cdsTranslationAray[i]);
            }
        }
    }

    /**
     *
     * @param genebankString the input string prints all CDS info
     */
    public void printCdsInfo(String genebankString) {
        String geneBlocks = geneBlockUnpacker(genebankString);
        String[] cdsTranslationAray = cdsTranslationAray(geneBlocks);
        String[] cdsDb_xrefAray = cdsDb_xrefAray(geneBlocks);
        String[] cdsProductAray = cdsProductAray(geneBlocks);
        String[] cdsCoordinatesAray = cdsCoordinatesAray(geneBlocks);
        String bundledCds = "";
        for (int i = 1; i < cdsCoordinatesAray.length - 1; i++) {
            System.out.println(cdsCoordinatesAray[i]);
            System.out.println(cdsProductAray[i]);
            System.out.println(cdsDb_xrefAray[i * 2] + "\n" + cdsDb_xrefAray[i * 2 + 1]);
            System.out.println(cdsTranslationAray[i] + "\n");
        }
    }

    /**
     *
     * @param genebankString the input string
     * @param startCoord the start coordinate
     * @param endCoord the end coordinate prints all features between the start
     * and stop coordinate
     */
    public void fetchFeatures(String genebankString, int startCoord, int endCoord) {
        String geneBlocks = geneBlockUnpacker(genebankString);
        String[] geneGene = geneGene(insertMissingGene(geneBlocks)).split("\n");
        String[] coordinates = geneCoordinates(geneBlocks).split("\n");
        for (int i = 1; i < coordinates.length; i++) {
            String[] looseCoords = stringManipulation.removeSignsFromString(stringManipulation.removeWordsFromString(coordinates[i])).split("\\.\\.");
            if (startCoord < Integer.parseInt(looseCoords[0]) && endCoord > Integer.parseInt(looseCoords[0])) {
                System.out.println("Found: " + geneGene[i] + " with " + looseCoords[0] + " and " + looseCoords[1] + " between given coordinates: " + startCoord + " " + endCoord);
            }
            if (Integer.parseInt(looseCoords[0]) > endCoord) {
                break;
            }
        }
    }

    /**
     *
     * @param genebankString
     * @return the gene coordinates
     */
    public String geneCoordinates(String genebankString) {
        return stringManipulation.stringCompare(genebankString, "( {5})" + "gene" + "(.*)", true, null);
    }

    /**
     *
     * @param genebankString the input string.
     * @return returns the average gene lenght in a double.
     */
    public double averageGeneLength(String genebankString) {
        String geneCoordinates = geneCoordinates(genebankString).replaceAll("[a-z]", "");
        geneCoordinates = geneCoordinates.replaceAll("\\.\\.", "x");
        geneCoordinates = geneCoordinates.replaceAll("\n", "x");
        geneCoordinates = geneCoordinates.replaceAll("\\W", "");
        String[] geneCoordinateAray = geneCoordinates.split("x");
        double[] geneCoordinateArayDouble = new double[geneCoordinateAray.length];
        Double totalGeneLength = 0.0;
        for (int i = 1; i < geneCoordinateAray.length; i++) {
            if (geneCoordinateAray[i].matches("\\d+")) {
                geneCoordinateArayDouble[i] = Integer.valueOf(geneCoordinateAray[i]);

            }
        }
        for (int i = 1; i < geneCoordinateArayDouble.length; i = i + 2) {
            totalGeneLength = totalGeneLength + (geneCoordinateArayDouble[i + 1] - geneCoordinateArayDouble[i]);
        }
        return (totalGeneLength / (geneCoordinateArayDouble.length / 2));
    }

    /**
     *
     * @param genebankString the input string.
     * @return the gene names
     */
    public String geneGene(String genebankString) {
        return stringManipulation.stringCompare(genebankString, "(.*)" + "/gene=" + "(.*)", true, null);
    }

    /**
     *
     * @param genebankString the input string.
     * @return the nucleotide sequence from the organism
     */
    public String sequence(String genebankString) {
        return stringManipulation.removeNumbersFromString(stringManipulation.stringCompare(genebankString, "ORIGIN" + "(.*)", true, "\\\\"));
    }
}
