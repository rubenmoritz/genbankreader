package genbankreader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author rjgmoritz A mostly generic String manipulation class for string
 * manipulation with genbank files
 */
public class StringManipulation {

    /**
     *
     * @param inputString the input string
     * @return returns the input string without numbers and non word characters.
     */
    public String removeNumbersFromString(String inputString) {
        String out;
        out = inputString.replaceAll("\\B*", "");
        out = out.replaceAll("\\d*", "");
        return out;
    }

    /**
     *
     * @param stringToSearchIn string you want to search in.
     * @param stringToSearchFor string to be found can be regex.
     * @function prints match and match location.
     */
    public void findStringInString(String stringToSearchIn, String stringToSearchFor) {
        Pattern pattern = Pattern.compile(stringToSearchFor);
        Matcher matcher = pattern.matcher(stringToSearchIn.toUpperCase());
        while (matcher.find()) {
            System.out.println("Found a match: " + (matcher.group()));
            System.out.println("Found a match at: " + (matcher.start() + 1));
        }
    }

    /**
     *
     * @param iUPACString input string
     * @return returns the input string as a regex pattern that matches the
     * IUPAC string in codons.
     */
    public String iUPACToRegex(String iUPACString) {
        String regexPattern = "";
        for (int i = 0; i < iUPACString.length(); i++) {
            if (iUPACString.substring(i, i + 1).equals("A")) {
                regexPattern = regexPattern + "A";
            }
            if (iUPACString.substring(i, i + 1).equals("C")) {
                regexPattern = regexPattern + "C";
            }
            if (iUPACString.substring(i, i + 1).equals("G")) {
                regexPattern = regexPattern + "G";
            }
            if (iUPACString.substring(i, i + 1).equals("T")) {
                regexPattern = regexPattern + "T";
            }
            if (iUPACString.substring(i, i + 1).equals("R")) {
                regexPattern = regexPattern + "[AG]";
            }
            if (iUPACString.substring(i, i + 1).equals("Y")) {
                regexPattern = regexPattern + "[CT]";
            }
            if (iUPACString.substring(i, i + 1).equals("S")) {
                regexPattern = regexPattern + "[GC]";
            }
            if (iUPACString.substring(i, i + 1).equals("W")) {
                regexPattern = regexPattern + "[AT]";
            }
            if (iUPACString.substring(i, i + 1).equals("K")) {
                regexPattern = regexPattern + "[GT]";
            }
            if (iUPACString.substring(i, i + 1).equals("M")) {
                regexPattern = regexPattern + "[AC]";
            }
            if (iUPACString.substring(i, i + 1).equals("B")) {
                regexPattern = regexPattern + "[CGT]";
            }
            if (iUPACString.substring(i, i + 1).equals("D")) {
                regexPattern = regexPattern + "[AGT]";
            }
            if (iUPACString.substring(i, i + 1).equals("H")) {
                regexPattern = regexPattern + "[ACT]";
            }
            if (iUPACString.substring(i, i + 1).equals("V")) {
                regexPattern = regexPattern + "[ACG]";
            }
            if (iUPACString.substring(i, i + 1).equals("N")) {
                regexPattern = regexPattern + "[ACGT]";
            }
            if (iUPACString.substring(i, i + 1).equals(".") || iUPACString.substring(i, i + 1).equals("-"));
            regexPattern = regexPattern + " ";
        }
        return regexPattern.replaceAll("\\s", "");
    }

    /**
     *
     * @param inputString the input string
     * @param stringToRemove the string to remove from the input string
     * @return the input string without the string that needed to be removed
     */
    public String removeStringFromString(String inputString, String stringToRemove) {
        String out;
        out = inputString.replaceAll(stringToRemove, "");
        return out;
    }

    /**
     *
     * @param inputString the input string
     * @return returns the input string without non letter characters.
     */
    public String removeNonLettersString(String inputString) {
        String out;
        out = inputString.replaceAll("[^a-zA-Z]", "");
        return out;
    }

    /**
     *
     * @param inputString the input string
     * @return returns the input string without words and all white spaces
     * except for \n
     */
    public String removeWordsFromString(String inputString) {
        String out;
        out = inputString.replaceAll("[a-zA-Z()]", "");
        out = out.replaceAll("[ \\t\\x0b\\r\\f]", "");
        return out;
    }

    /**
     *
     * @param inputString the input string
     * @return returns the input string without <> and ()
     */
    public String removeSignsFromString(String inputString) {
        String out;
        out = inputString.replaceAll("[()<>]", "");
        return out;
    }

    /**
     *
     * @param inputString the input string
     * @param stringToRemove string to remove in regex.
     * @return the input string without the lines you want to remove.
     */
    public String removeLinesFromString(String inputString, String stringToRemove) {
        String[] instr = inputString.split("\n");
        String out = "";
        for (int i = 0; i < instr.length; i++) {
            if (instr[i].matches(stringToRemove)) {
                instr[i] = "";
            }
            out = out + instr[i];
        }

        return out;
    }

    /**
     *
     * @param stringToSearchIn a string that is being searched in.
     * @param recognitionString a Regex pattern or string that is be in searched
     * for.
     * @param multipleInstances a boolean that if true makes the search go on
     * after one find and if false stops the code for looking for more.
     * @param stopString a string or single character that gets each line after
     * the recognitionString until the stopString has been found.
     * @return returns the string that is being looked for.
     */
    public String stringCompare(String stringToSearchIn, String recognitionString, Boolean multipleInstances, String stopString) {
        String[] gbAray = stringToSearchIn.split("\n");
        String outputString = "";
        for (int i = 0; gbAray.length > i; i++) {
            String patern = recognitionString;
            if (gbAray[i].matches(patern)) {
                outputString = outputString + "\n" + gbAray[i];
                if (stopString != null) {
                    int j = 0;
                    do {
                        j++;
                        if (gbAray[i + j].matches("(.*)" + recognitionString + "(.*)" + stopString)) {
                            break;
                        }
                        if (j > 0) {
                            outputString = outputString + "\n" + gbAray[i + j];
                        }
                    } while (!gbAray[i + j].matches(stopString) && gbAray.length >= i + j + 2);

                }
            }
            if (!multipleInstances && !"".equals(outputString)) {
                return outputString;
            }
        }
        return outputString;

    }
}
