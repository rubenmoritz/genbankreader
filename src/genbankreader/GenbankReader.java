package genbankreader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rjgmoritz (rubenmoritz@gmail.com) A genbank reader
 */
public class GenbankReader {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Organism organism = new Organism();
        GenbankReader genbankReader = new GenbankReader();
        StringManipulation stringManipulation = new StringManipulation();
        int argumentNumber = 0;
        String genebankString = null;
        for (String argument : args) {
            argumentNumber++;
            if (argument.contains("--help")) {
                System.out.println("This is the help info\n"
                        + "use --infile for input file\n"
                        + "use --infile <input file>­‐summary for a summary about the file\n"
                        + "use --infile <input file>­‐find_sites with the IUPAC code you want to find for the locations of these sequences\n"
                        + "use --infile <input file>­‐fetch_gene with the gene name you want to find for the nucleotide sequences of this gene\n"
                        + "use --infile <input file>­‐fetch_cds with the product name you want to find for the aminoacid sequences of these\n"
                        + "use --infile <input file>­‐fetch_features with the coordinates you want to look between for features\n");
            }
            if (argument.contains("--infile")) {
                System.out.println(args[argumentNumber] + " is the input file name");
                String inputFile = args[argumentNumber];
                genebankString = genbankReader.readFile(inputFile);
            }
            if (argument.contains("--summary")) {
                System.out.println("***FILE SUMMARY***\n\n"
                        + "Organism: " + stringManipulation.removeStringFromString(organism.sourceOrganism(genebankString), "(.*)organism=") + "\n"
                        + "Sequence length: " + stringManipulation.removeNonLettersString(organism.sequence(genebankString)).length() + "\n"
                        + "Average gene length " + organism.averageGeneLength(genebankString));

            }
            if (argument.contains("--find_sites")) {
                String iUPACString = args[argumentNumber];
                String organismSeq = organism.sequence(genebankString).replaceAll("[^ACTGactg]", "");
                System.out.println(organismSeq.length());
                stringManipulation.findStringInString(organismSeq, stringManipulation.iUPACToRegex(iUPACString));
            }
            if (argument.contains("--fetch_gene")) {
                organism.fetchGenes(genebankString, args[argumentNumber]);
            }
            if (argument.contains("--fetch_cds")) {
                organism.fetchCds(genebankString, args[argumentNumber]);
            }
            if (argument.contains("--fetch_features")) {
                organism.fetchFeatures(genebankString, Integer.parseInt(args[argumentNumber]), Integer.parseInt(args[argumentNumber + 1]));
            }
        }

    }

    /**
     *
     * @param inputFile
     * @return returns the whole file in a string
     */
    private String readFile(String inputFile) {
        String wholeFile = "";
        try {
            File input = new File(inputFile);
            Scanner scanner;
            scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                wholeFile = wholeFile + "\n" + scanner.nextLine();
            }
            scanner.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GenbankReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return wholeFile;
    }
}
